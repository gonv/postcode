import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
 
  baseURL: string = 'https://api.postcodes.io/postcodes/';
  
  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };


  constructor(private http: HttpClient) { }

  getSearchData(postcode: string): Observable<any>{
    return this.http.get(this.baseURL + postcode)
  }
}
