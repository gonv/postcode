import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {

  searches     : any = [];
  postcode     : string = '';
  latitude     : any = [];
  longitude    : any = [];
  dist_mi      : any;
  dist_km      : any;
  errorEmpty   : string = 'Please insert a Postcode';
  errorInvalid : string = 'Invalid Postcode';
  
  searchHistory: any [] = [
    {
       searchId: '1', postcode: 'OX495NU', timestamp: "2007-04-30 13:10:02.047" 
    }
  ];  

  constructor(
  private _http: SearchService
    ) { }

  public getSearchData() {
      this._http.getSearchData(this.postcode).subscribe(
        (res) => {
          this.searches [0] = res;
          this.latitude  = res.result.latitude;
          this.longitude = res.result.longitude;
          this.distanceToHeathrow();
      },
      (error) => {
          if (error.status == "400") {
            this.searches [0] = this.errorEmpty;
          }

          if (error.status == "404") {
            this.searches [0] = this.errorInvalid;
          }
      });
  }

  public distanceToHeathrow()
{
    let lat_heathrow = 51.470020;
    let lng_heathrow = -0.454295;
    let lat = (this.latitude - lat_heathrow) * (Math.PI / 180);
    let lng = (this.longitude - lng_heathrow) * (Math.PI / 180);
    let calc = Math.sin(lat / 2) * Math.sin(lat / 2) +
                  Math.cos(lat_heathrow * (Math.PI / 180)) * Math.cos(this.latitude  * (Math.PI / 180)) *
                  Math.sin(lng / 2) * Math.sin(lng / 2);
    let result = 2 * Math.asin(Math.min(1, Math.sqrt(calc)));
    this.dist_mi = (3960 * result).toPrecision(8);
    this.dist_km = (6371 * result).toPrecision(8);
}
}