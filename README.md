# Full Stack Challenge

### Let’s pretend you are starting your first day with us. After you settle in, your first task will be assigned. Both Joseph (Product Owner) and Ness (Technical Lead) are ready to give you more details on your first task. ###

* Joseph - “Hi, I have your first task, excited?”

* “We need to create a service that allows you to find the addresses of our customers based on their postcodes. Multiple addresses will be searched, so it would be nice if we could have a history endpoint. Finally, we need to display the distance in a straight line, from the customer location to London Heathrow airport (lat/long: 51.4700223,-0.4542955) this should be displayed in both kilometers and miles.”

* Ness - “Ok, I understand the request and I think I can provide some help. I’ve heard about https://postcodes.io/, and we can use it as web service where we can type the postcode and we get the address details back, alongside the latitude and longitude. As its straight line it should be very easy to calculate the distance between the airport and the customer address.

* Joseph - “For the web, we will need a web page that allows to search for postcodes, providing feedback to the user about the postcode and the distance to London Heathrow airport”

### A working example for the web service is: http://api.postcodes.io/postcodes/N76RS

## A few examples of valid postcodes in the UK are:
PostCode
N76RS
SW46TA
SW1A
W1B3AG
PO63TD

## **A api using .Net Framework or .Net Core is required**

I think using angular for the web application would be an excellent idea! Obviously if you want to use other frontend tools/technologies to help you achieve the work, I’m happy with it, but remember the focus must be as much in having it working as it should to have a good user experience.

Now that the exercise has been explained, you can start working on it, we normally request for it to be uploaded into our git repository within 3 working days, but can be changed if you need more time to start. This task should take you no longer than 5 hours and needless to say should be totally completed by you, after all this is supposed to be a fun challenge! 

Once you are done please commit and push the code to the repository so we can review it.
If you're not familiar with GIT, use the Sourcetree client to get started or visit http://git-scm.com for the official git client.
The PUSH with notify us and we will review your work. Only push when you're done, and push once. This is to avoid us reviewing an incomplete submission.


# Postcode Project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

